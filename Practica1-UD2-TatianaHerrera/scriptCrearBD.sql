CREATE DATABASE fruteria;

use fruteria;

CREATE TABLE frutas(

id int primary key auto_increment,
nombre varchar(30) unique,
tipo varchar (40),
origen varchar (40),
clasificacion varchar (40),
kilos varchar (10),
fecha_caducidad timestamp
);

INSERT INTO frutas(nombre, tipo, origen, clasificacion, kilos, fecha_caducidad)
VALUES ('Fresa','Oso Grande', 'California','Fragariinae', 'tres', '20-04-2021');

select * from frutas;