DELIMITER //
CREATE PROCEDURE crearTablaFrutas()
BEGIN
    CREATE TABLE frutas(
                           id int primary key auto_increment,
                           nombre varchar(30) unique,
                           tipo varchar (40),
                           origen varchar (40),
                           clasificacion varchar (40),
                           kilos varchar (10),
                           fecha_caducidad timestamp);
END //