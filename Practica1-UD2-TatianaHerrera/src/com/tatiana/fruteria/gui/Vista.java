package com.tatiana.fruteria.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
    JTextField txtNombre;
    JTextField txtTipo;
    JTextField txtOrigen;
    JTextField txtClasificacion;
    JTextField txtKilos;
    JButton btnNuevo;
    JButton btnBuscar;
    JButton btnEliminar;
    JTextField txtBuscar;
    JTable tabla;
    DateTimePicker dateTimePicker;
    JLabel lblAccion;
    JButton btnfrutasTipo;
    private JTable frutasTipo;

    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    JMenuItem itemConectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;
    JFrame frame;

    public Vista(){
        frame= new JFrame("FRUTERIA");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm= new DefaultTableModel();
        tabla.setModel(dtm);
        dtm1= new DefaultTableModel();
        frutasTipo.setModel(dtm1);

        crearMenu();

        frame.pack();
        frame.setVisible(true);
    }

    private  void crearMenu(){
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Frutas");
        itemCrearTabla.setActionCommand("CrearTablaFrutas");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);

    }
}
